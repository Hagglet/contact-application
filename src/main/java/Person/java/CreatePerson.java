package Person.java;

import com.github.javafaker.Faker;

import java.util.Date;

public class CreatePerson {
    private String fullName;
    private String address;
    private String mobileNumber;
    private String workNumber;
    private Date birthDay;

    public CreatePerson() {
        Faker faker = new Faker();
        this.fullName = faker.name().fullName();
        this.address =  faker.address().fullAddress();
        this.mobileNumber = faker.phoneNumber().phoneNumber();
        this.workNumber = faker.phoneNumber().phoneNumber();
        this.birthDay = faker.date().birthday();
    }
    public CreatePerson(String fullName,String address,String mobileNumber,String workNumber,Date birthDay) {
        this.fullName = fullName;
        this.address =  address;
        this.mobileNumber = mobileNumber;
        this.workNumber = workNumber;
        this.birthDay = birthDay;
    }
    public String getFullInformation() {
        return fullName + "\n" + "\n" + address + "\n" + birthDay +  "\n" + mobileNumber + "\n" + workNumber;
    }
    public String getFullName() {
        return fullName;
    }
    public String getAddress() {
        return address;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getWorkNumber() {
        return workNumber;
    }

}
