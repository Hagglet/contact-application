import Person.java.CreatePerson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.*;
import java.util.*;
import java.util.ArrayList;
/*
This program will create a JSON file and populate it with auto generated content
When a user wanna do a search the content from the JSON file will be loaded in to arraylist
The user can then do a search after a word
 */


public class Program {
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";
    private static ArrayList<CreatePerson> persons;
    public static void main(String[] args) {
        System.out.println("If this is the first time using this program you need to populate a file. " +
                "You do that by pressing 2");
        mainMenu();

    }
    /*
    This function read in the content from the JSON file and puts it in a ArrayList
    When the content is loaded in to the Arraylist it will let you do a search after a word
    It will only print the content that contains the word and will highlight that word with red
    The function will then call for the mainMEnu function for further search
     */

    public static void loadContentFromJsonAndSearchAfterWords() {
        Gson gson = new Gson();
        Scanner sc = new Scanner(System.in);
        persons = new ArrayList<>();
        File jsonFilePath = new File("person.json");
        if (jsonFilePath.length() == 0) {
            System.out.println("The file is empty");
            mainMenu();
        }
        try {
            FileReader reader = new FileReader(jsonFilePath);
            persons = gson.fromJson(reader, new TypeToken<ArrayList<CreatePerson>>() {
            }.getType());

            reader.close();
        } catch (Exception e) {
            System.out.println("someting went wrong");
        }

        System.out.println("Write in what you wanna search after");
        String searchWord = sc.nextLine();
        for (CreatePerson person : persons) {

            String stringPerson = person.getFullInformation();
            if (stringPerson.contains(searchWord)) {
                System.out.println(person.getFullInformation().replace(searchWord, ANSI_RED + searchWord + ANSI_RESET));
                System.out.println();
            }
        }
        mainMenu();
    }
    /*
    Write in new content to the json file
    Generates 20 new persons and add them to the Arraylist it
    creates a new JSON object and write it to the JSON file
     */
    public static void writeNewContentToJSON() {
        ArrayList<CreatePerson> list = new ArrayList<>();

        for (int i = 0; i < 20; i++) {

            list.add(new CreatePerson());
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        File f;
        FileWriter writeToFile;
        try {
            f = new File("person.json");
            writeToFile = new FileWriter(f);

            if (!f.exists()) {
                f.createNewFile();
            }
            gson.toJson(list, writeToFile);
            writeToFile.close();
            System.out.println("New content has been loaded");
            mainMenu();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
    Menu that will show the options that can be made in the program
     */

    public static void mainMenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println();
        System.out.println("1. Wanna do a search " + "\n" + "2. Load new content" + "\n" + "" +
                "3. Exit the program");
        while (!sc.hasNextInt()) {
            System.out.println("Wrong input");
            sc.nextLine();
            mainMenu();
        }
        int choice = sc.nextInt();
        if (choice == 1) {
            loadContentFromJsonAndSearchAfterWords();
        }
        if (choice == 2) {
            writeNewContentToJSON();
        }
        if (choice == 3) {
            System.exit(1);
        }
    }


}